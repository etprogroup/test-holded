# Holded : Technical Test: Full Stack Developer

## Install Environment (Previously)

If you have installed `composer`, `mongodb` and `librariy mongodb for php`. You should install the project now.

### Install Composer
- [Download and install composer](https://getcomposer.org/Composer-Setup.exe)


### Install mongodb 
- [Download version from here](https://www.mongodb.com/download-center/community?jmp=docs)
- [The folow instructions for installation](https://docs.mongodb.com/manual/administration/install-community/)


### Install library php by mongo 
- [install library php-library for mongodb](https://docs.mongodb.com/php-library/master/)


## Install project

### Download project

- [Download from git](git clone https://etpro@bitbucket.org/etprogroup/test-holded.git)

> `git clone https://etpro@bitbucket.org/etprogroup/test-holded.git` 

Then clone the repository in with you sourcetree or other program for git repositories. if not use programs, you can use command line as: 

- cd [path/project]
- git remote add origin `[url/repository]`. *Example:* `git remote add origin https://etpro@bitbucket.org/etprogroup/test-holded.git`
- git pull origin master

`Important:` You should install git previously, if you don't have it. [Install git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

- In command line from project path, write:

> `cd [path/project]` and then `composer install`

if have any version problem or requirement, write: `composer install --ignore-platform-reqs`

### Config project `important`

Open the file `/common/config/main.php` and:
 
- Change the variable global `_base` with the directory based where configure your localhost domain. *Example:* `http://local.holded.com` is `define(_base, "/")`, if you folder in local is  `http://local.holded.com/[folder/base]` is `define(_base, "/[folder/base]/")` _(This is for show the css correctly)_
- Change the variable global `_mongo_uri` with the host and port of you mongo. *Example:* `/localhost:27017`
- Change the variable global `_mongo_database` with the database name of you project in mongo.
- Change the variable global `_mongo_collection` with the collection name of you project in mongo.

Open the `/admin/.htaccess` base project and:

- Change `RewriteBase /admin/` per your directory base project Example]: `RewriteBase /[folder/base]/admin/`


## Then see in the project

- In the host base http://local.holded.com`/`: your see the html and css configuration from junior test.

- In http://local.holded.com`/admin/users/`: your see the list user requested in the senior test, with: 

    - Add new user
    - Edit user pushing in the `pencil` 
    - Remove user pushing in the `X`
    
[Link test](http://local.holded.com/admin/users/) if you localhost domain is _http://local.holded.com_
    
## More 

In the project, I don't use frameworks or libraries like jQuery, bootstrap, DataTable, fontAwesome or material design to show you that I understand in pure code.

For less development time and more quality results, is better to use these tools