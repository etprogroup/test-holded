<?php
namespace admin\controllers;

use common\library\Controller;
use MongoDB\BSON\ObjectID;

class UsersController extends Controller {

    /**
     * Show list users
     * @return mixed
     */

    public function view(){
        $company = isset($_SESSION['company']) ? $_SESSION['company'] : 0;
        $users = $this->connect()->find(['company' => $company]);
        return $this->render('view', [
            'users' => $users
        ]);
    }

    /**
     * Add user by id and company (redirect to edit for use method properties)
     * @return mixed|void
     */

    public function add(){
        return $this->edit(null);
    }

    /**
     * Add and Edit user by id and company
     * @param null $id
     * @return mixed|void
     */

    public function edit($id = null){
        $user = null;
        $connect = $this->connect();
        $company = isset($_SESSION['company']) ? $_SESSION['company'] : 0;

        if(isset($_POST['user'])){
            $_POST['user']['company'] = $company;

            if($_FILES && isset($_FILES['picture'])){
                $pictureInfo = pathinfo($_FILES['picture']['name']);
                if(isset($pictureInfo['extension'])){
                    $pictureExt = $pictureInfo['extension'];
                    $pictureName = "file-".time()."-$company.$pictureExt";
                    $picturePath = "uploads/$pictureName";

                    $target = dirname(dirname(__DIR__)) .'/'. $picturePath;
                    move_uploaded_file( $_FILES['picture']['tmp_name'], $target);
                    $_POST['user']['picture'] = $picturePath;
                }
            }

            if($id){
                //update user
                $connect->findOneAndUpdate([
                    '_id' => new ObjectID($id),
                    'company' => $company
                ], [
                    '$set' => $_POST['user']
                ]);

            }else{
                //insert user
                $connect->insertOne($_POST['user']);
            }

            return $this->redirect('');
        }

        if($id){
            $user = $connect->findOne([
                '_id' => new ObjectID($id),
                'company' => $company
            ]);
        }

        return $this->render('edit', [
            'user' => $user
        ]);
    }

    /**
     * Delete user by id and company
     * @param null $id
     */
    public function delete($id = null){
        $connect = $this->connect();
        $company = isset($_SESSION['company']) ? $_SESSION['company'] : 0;

        if($id){
            $connect->findOneAndDelete([
                '_id' => new ObjectID($id),
                'company' => $company
            ]);
        }

        return $this->redirect('');
    }

    /**
     * Change Company
     * @param null $id
     */
    public function company($id = null){
        $_SESSION['company'] = $id;
        return $this->redirect('');
    }
}