<?php

namespace admin\modules;

if(!isset($_SESSION)){
    session_start();
}

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once(dirname(dirname(__DIR__)) . '/vendor/autoload.php');
include_once(dirname(dirname(__DIR__)) . '/common/config/main.php');

$controller = isset($_GET['controller']) ? $_GET['controller'] : 'users';
$action = isset($_GET['action']) ? $_GET['action'] : 'view';
$id = isset($_GET['id']) ? $_GET['id'] : null;
define('_section', 'admin');
define('_controller', $controller);
define('_action', $action);

$controllerName = ucfirst(strtolower($controller))."Controller";
$controllerNameSpace = "\\admin\\controllers\\$controllerName";

$controller = new $controllerNameSpace();

if($id){
    $controller->$action($id);
}else{
    $controller->$action();
}