<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" class="">

<head>
    <base href="<?= _base ; ?>"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">

    <!-- styles -->
    <link href="assets/css/style.css" rel="stylesheet"/>
    <link href="" type="imag/png" rel="shortcut icon"/>

    <title>DASHBOARD</title>
</head>

<body>
<div class="page">

    <header class="section">
        <div class="">
            <div class="">
                <div class="icon" style="font-size: 40px;">&#9786;</div>
                <div style="display: inline-block; font-size: 12px;">Holded<br><strong>Invoicing</strong></div>
                <div class="icon" style="margin-left: 50px; font-size: 25px;">&#9776;</div>
            </div>
        </div>
        <div class="box-icon">
            <div class="icon" style="color:#FFF; font-size: 20px;">&#43;</div>
        </div>
        <div class="" style="float: right;">
            <div class="icon grey">&#128269;</div>
            <div class="icon grey">&#11095;</div>
            <div class="icon grey">&#9733;</div>
            <div class="icon grey" style="color: #e65d4b;">&#128276;</div>
            <div class="icon grey">&#9881;</div>
            <div class="box-icon" style="margin-left: 20px; width: 30px; background: #e65d4b;">
                <div class="icon" style="color:#FFF; font-size: 15px;">&#128077;</div>
            </div>
            <div class="icon grey" style="font-size: 40px; border-left: 1px solid #CCC;">&#9786;</div>
        </div>
    </header>

    <div class="page-center">
        <nav class="menu-lateral" style="width: 65px;">
            <div class="menu-item box-icon square selected">
                <div class="icon">&#128077;</div>
            </div>
            <div class="menu-item box-icon square">
                <div class="icon">&#128077;</div>
            </div>
            <div class="menu-item box-icon square">
                <div class="icon">&#128077;</div>
            </div>
            <div class="menu-item box-icon square">
                <div class="icon">&#128077;</div>
            </div>
            <div class="menu-item box-icon square">
                <div class="icon">&#128077;</div>
            </div>
            <div class="menu-item box-icon square">
                <div class="icon">&#128077;</div>
            </div>
            <div class="menu-item box-icon square">
                <div class="icon">&#128077;</div>
            </div>
            <div class="menu-item box-icon square">
                <div class="icon">&#128077;</div>
            </div>
            <div class="menu-item box-icon square">
                <div class="icon">&#128077;</div>
            </div>
            <div class="menu-item box-icon square">
                <div class="icon">&#128077;</div>
            </div>
            <div class="menu-item box-icon square">
                <div class="icon">&#128077;</div>
            </div>
        </nav>
        <div class="center">
            <?= $content ?>
        </div>
    </div>
</div>

</body>
</html>