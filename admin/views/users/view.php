<?php

$jsonUsers = [];
foreach ($users as $user) {

    $hours = 0;
    $timeEat = 1;
    foreach ($user->schedule as $day) {
        $timeEnd = !empty($day->end) ? str_replace(":", ".", $day->end) : 0;
        $timeStart = !empty($day->start) ? str_replace(":", ".", $day->start) : 0;
        $timeDifference = $timeEnd - $timeStart;
        $timeDifferenceEat = $timeDifference<7 ? 0 : $timeEat;
        $hours = $hours + $timeDifference - $timeDifferenceEat;
    }

    $fullName = $user->name->first_name.' '.$user->name->last_name;

    //set options
    $actions = '<div class="icon" title="Editar '.$fullName.'" ><a href="'._base.'admin/users/edit/'.$user->_id.'">&#9998;</a></div>';
    $actions .= '<div class="icon" title="Eliminar '.$fullName.'"><a href="'._base.'admin/users/delete/'.$user->_id.'">x</a></div>';

    $jsonUsers[] = [
        ($user && isset($user->picture) && !is_null($user->picture) && !empty($user->picture) ? '<img src="'._base.$user->picture.'" style="height: 50px; border-radius: 5px;" />' : ''),
        $user->name->first_name,
        $user->name->last_name,
        $user->email,
        $user->position,
        $user->office,
        $user->salary,
        $hours,
        $actions,
    ];
}

?>
<div class="contain">
    <div class="" style="text-align: right;">
        <select id="company" name="company" onchange="changeCompany();">
            <?php
            $company = isset($_SESSION['company']) ? $_SESSION['company'] : 0;
            foreach(_companies as $companyKey => $companyName){
                echo '<option value="'.$companyKey.'" '.($company == $companyKey ? 'selected' : '').'>'.$companyName.'</option>';
            }?>
        </select>
    </div>

    <div class="box-icon square button">
        <a href="<?= _base.'admin/users/add/' ?>">
            <div class="icon">Nuevo</div>
        </a>
    </div>

    <div class="box" style="width: 100%;">
        <table id="tableUser" cellpadding="0" cellspacing="0">
            <thead>
            <tr>
                <th></th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Email</th>
                <th>Cargo</th>
                <th>Centro de trabajo</th>
                <th>Salario</th>
                <th>Horas Semanales</th>
                <th>Acciones</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
        <table>
        <tfoot>
        <tr>
            <td style="text-align: left;">
                <div class="navigate">
                    <div id="tablePageCurrent" class="icon" style="font-weight: bold;"></div>
                    <div id="tablePageTotal" class="icon"></div>
                </div>
            </td>

            <td style="text-align: right;">
                <div class="navigate">
                    <div onclick="changePage(-1)" class="icon"> &#60; </div>
                    <div id="tablePage" class="icon"></div>
                    <div onclick="changePage(1)" class="icon"> &#62; </div>
                </div>
            </td>
        </tr>
        </tfoot>
        </table>
    </div>
</div>

<script>
    var page = 0;
    var limit = 10;
    var users = <?= json_encode($jsonUsers, JSON_FORCE_OBJECT); ?>;
    var table = document.querySelector('#tableUser tbody');
    var total = Object.keys(users).length;
    console.log(users);

    function fillTable() {
        var init = page * limit;
        var end = init + limit;
        var index = 0;

        if (total >= init) {
            table.innerHTML = '';
            Object.keys(users).map(function (user) {
                if (index >= init && index < end) {
                    var tr = document.createElement('tr');
                    table.appendChild(tr);

                    Object.keys(users[user]).map(function (value) {
                        var tdElement = document.createElement('td');
                        tdElement.innerHTML = users[user][value];
                        tr.appendChild(tdElement);
                    });
                }

                index += 1;
            });
        }
    }

    function changePage(direction) {
        if (direction > 0) {
            page += 1;

        } else {
            page -= 1;
        }

        if (page < 0) {
            page = 0;

        } else if (page > (total / limit) - 1) {
            page = Math.ceil((total / limit) - 1);
        }

        page = Math.floor(page);

        fillTable();
        fillPage();

    }

    function fillPage(direction) {
        var tablePage = document.getElementById('tablePage');
        tablePage.innerHTML = page+1;

        var numLimit = (page*limit)+limit;
        var tablePageCurrent = document.getElementById('tablePageCurrent');
        tablePageCurrent.innerHTML = ((page*limit)+1)+' - '+(numLimit > total ? total : numLimit);

        var tablePageTotal = document.getElementById('tablePageTotal');
        tablePageTotal.innerHTML = '('+total+')';
    }

    fillTable();
    fillPage();

    function changeCompany(id){
        var company = document.getElementById('company').value;
        window.location = '<?= _base ?>admin/users/company/'+company;
    }
</script>