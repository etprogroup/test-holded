<?php

$jsonUsers = [];
$weekDays  = [
    'monday' => 'Lunes',
    'tuesday' => 'Martes',
    'wednesday' => 'Miercoles',
    'thursday' => 'Jueves',
    'friday' => 'Viernes',
    'saturday' => 'Sabado',
    'sunday' => 'Domingo',
];

?>
<div class="contain">
    <div class="box" style="width: 100%;">
        <form action="" method="post" enctype="multipart/form-data">

            <div class="box-input" style="display: block;">
                <label>Cambiar Imagen</label>
                <input id="userImage" type="file" name="picture" style="<?= $user && $user->picture ? 'display: none;' : '' ?>"/>
                <?= $user && isset($user->picture) && !is_null($user->picture) && !empty($user->picture) ? '<img src="'._base.$user->picture.'" style="height: 100px; border-radius: 20px;" onclick="changeImage()"/>' : '' ?>
            </div>

            <div class="box-input">
                <label>Nombre</label>
                <input type="text" name="user[name][first_name]" value="<?= $user && $user->name ? $user->name->first_name : '' ?>"/>
            </div>

            <div class="box-input">
                <label>Apellido</label>
                <input type="text" name="user[name][last_name]" value="<?= $user && $user->name ? $user->name->last_name : '' ?>"/>
            </div>

            <div class="box-input">
                <label>Email</label>
                <input type="text" name="user[email]" value="<?= $user ? $user->email : '' ?>"/>
            </div>

            <div class="box-input">
                <label>Cargo</label>
                <input type="text" name="user[position]" value="<?= $user ? $user->position : '' ?>"/>
            </div>

            <div class="box-input">
                <label>Centro de trabajo</label>
                <input type="text" name="user[office]" value="<?= $user ? $user->office : '' ?>"/>
            </div>

            <div class="box-input">
                <label>Salario</label>
                <input type="text" name="user[salary]" value="<?= $user ? $user->salary : '' ?>"/>
            </div>

            <div class="box-input" style="width: calc(100% - 25px);">
                <label>Horario Semanal</label>
                <div class="schedule">
                    <?php foreach($weekDays as $dayKey => $dayValue){?>
                        <div class="box-input">
                            <label><?= $dayValue ?></label>
                            <div class="box-input">
                                <label>Inicio</label>
                                <input type="text" name="user[schedule][<?= $dayKey ?>][start]" value="<?= $user && isset($user->schedule->{$dayKey}) ? $user->schedule->{$dayKey}->start : '' ?>" placeholder="8:00"/>
                            </div>
                            <div class="box-input">
                                <label>Fin</label>
                                <input type="text" name="user[schedule][<?= $dayKey ?>][end]" value="<?= $user && isset($user->schedule->{$dayKey}) ? $user->schedule->{$dayKey}->end : '' ?>" placeholder="17:00"/>
                            </div>
                          </div>
                    <?php  } ?>
                </div>
            </div>

            <div class="box-icon square button">
                <button class="icon" type="submit" style="background: none; border: none;"><?= $user ? 'Actualizar' : 'Guardar' ?></button>
            </div>

        </form>
    </div>
</div>

<script>
    function changeImage(){
        var file = document.getElementById('userImage');
        file.click();
    }
</script>