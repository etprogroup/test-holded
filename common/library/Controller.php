<?php
namespace common\library;

use MongoDB\Client;
use MongoDB\Collection;
use MongoDB\Driver\Manager;
use MongoDB\Driver\ReadPreference;
use MongoDB\Driver\Server;

class Controller{

    public function render($view, $_params_){
        $class = get_class($this);
        $reflector = new \ReflectionClass($class);
        $fn = $reflector->getFileName();
        $dir = dirname($fn);

        $layout =dirname($dir)."/views/index.php";
        $file =dirname($dir)."/views/"._controller."/$view.php";
        if(file_exists($file)){
            if(is_array($_params_)){
                extract($_params_);
            }

            ob_start();
            require_once($file);
            $content = ob_get_contents();
            ob_end_clean();

            return require_once($layout);

        }else{

            echo "not exist file $file";
        }
    }

    public function redirect($view){
        header('Location: '._base._section.'/'._controller.'/'.$view);
    }

    public function connect(){
        $mongoClient = new Client(_mongo_uri);
        $colección = $mongoClient->selectCollection(_mongo_database, _mongo_collection);
        return $colección;
    }


}