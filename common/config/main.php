<?php
$config = [];
define('_base', "/");

define('_companies', [
    0 => 'Company 1',
    1 => 'Company 2',
    2 => 'Company 3'
]);

define('_mongo_uri', "mongodb://localhost:27017/");
define('_mongo_database', 'holded');
define('_mongo_collection', 'collection');